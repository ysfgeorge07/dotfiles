 ```
  ____        _    __ _ _
|  _ \  ___ | |_ / _(_) | ___  ___
| | | |/ _ \| __| |_| | |/ _ \/ __|
| |_| | (_) | |_|  _| | |  __/\__ \
|____/ \___/ \__|_| |_|_|\___||___/
  ```
# Table of content
- Window Manager
  + [DWM]()
- Terminal
  + [ST]()
  + [alacritty]()
- Editor
  + [Neovim]()
- Run Luancher
  + [Dmenu]()
- Compositor
  + [Picom]()
- Shell
  + [Zshrc]()


# Credits
  + DT
  + Chris@Machine

# LICENSE
  - MIT
